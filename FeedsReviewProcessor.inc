<?php

/**
 * @file
 * Class definition of FeedsReviewProcessor.
 */

/**
 * Creates nodes from feed items.
 */
class FeedsReviewProcessor extends FeedsProcessor {

  /**
   * Yo dawg! I heard you like processors. So we put a processor in your processor so you can process after you've processed!
   */
  protected $processor;

  /**
   * Ensure that Ajax didn't kill the importer.
   *
   * Important:
   * These two lines are here to fix an issue with Ajax loading in this form.
   * The FeedsConfigurable::instance() method uses a static cache to keep track of the importer class.
   * On a non-Ajax call it's fine, because that would have been initialized by the page callback.
   * With Ajax, however, it hasn't been initialized, so the first call to it doesn't happen until the
   * FeedsPlugin::save() delegates up to the importer to save, which doesn't exist yet, so it spawns a new one which
   * isn't configured.
   *
   * Used in:
   *   $this->configForm()
   *   $this->configFormSubmit()
   *
   * @return
   *   Returns the initialized importer.
   */
  protected function ensureImporter() {
    return feeds_importer($this->id);
  }

  /**
   * Override parent::setConfig().
   */
  public function setConfig($config) {

    // Run the parent method.
    parent::setConfig($config);

    // If a processor is chosen, load it into $this->processor.
    if (!empty($config['processor'])) {

      // Load the processor.
      $this->processor = feeds_plugin($config['processor'], $this->id);

      // Set the config.
      $this->processor->setConfig($this->config['processor_settings']);
    }
  }

  /**
   * Override parent::entityType().
   */
  public function entityType() {
    return 'feeds_review_item';
  }

  /**
   * Override parent::newEntity().
   */
  protected function newEntity(FeedsSource $source) {
    $item = feeds_review_item_new();
    return $item;
  }

  /**
   * Override parent::entityLoad().
   */
  protected function entityLoad(FeedsSource $source, $entity_id) {
    if ($this->config['update_existing'] == FEEDS_UPDATE_EXISTING) {
      $item = feeds_review_item_load($entity_id);
    }
    else {
      $item = feeds_review_item_new();
    }
    return $item;
  }

  /**
   * Override parent::entitySave().
   */
  protected function entitySave($entity) {
    feeds_review_item_save($entity);
  }

  /**
   * Override parent::entityDeleteMultiple().
   */
  protected function entityDeleteMultiple($entity_ids) {
    feeds_review_item_delete_multiple($entity_ids);
  }

  /**
   * Override parent::process().
   *
   * This function is similar to it's parent, but simplified.
   * It just loops through the items and adds them as FeedsReviewItem entities.
   * No mapping is performed (that will be done during the actual processing later).
   */
  public function process(FeedsSource $source, FeedsParserResult $parser_result) {
    $state = $source->state(FEEDS_PROCESS);

    // Loop through the available items, and pop them off one by one for processing.
    while ($item = $parser_result->shiftItem()) {
      try {

        // Build a new entity.
        $entity = $this->newEntity($source);
        $this->newItemInfo($entity, $source->feed_nid);

        // Set the entity values.
        global $user;
        $entity->uid = $user->uid;
        $entity->data = serialize($item);

        // We use the {feeds_review_import} table to keep track of imports that
        // are run, including the configuration of the importer at the time of
        // import. There are two ways that the database record is created:
        //   1) If feeds_review_batch_import() is run.
        //   2) In the code below, if it isn't already set.
        $processor_config = $source->importer->processor->getConfig();
        if (empty($processor_config['feeds_review']['import_id'])) {

          // Create a new import record, saving the id of the current importer, and the id of the file being imported (if available).
          $fid = !empty($source->config['FeedsFileFetcher']['fid']) ? $source->config['FeedsFileFetcher']['fid'] : NULL;
          $import = feeds_review_import_record($source->id, $fid);

          // Save the import id to the processor config.
          $processor_config['feeds_review']['import_id'] = $import['id'];
          $source->importer->processor->setConfig($processor_config);
        }

        // Assign the item's import id.
        $entity->import_id = $processor_config['feeds_review']['import_id'];

        // Save the entity.
        $this->entitySave($entity);
        $state->created++;
      }
      catch (Exception $e) {
        $state->failed++;
        drupal_set_message($e->getMessage(), 'warning');
        $message = $e->getMessage();
        $message .= '<h3>Original item</h3>';
        $message .= '<pre>' . var_export($item, TRUE) . '</pre>';
        $message .= '<h3>Entity</h3>';
        $message .= '<pre>' . var_export($entity, TRUE) . '</pre>';
        $source->log('import', $message, array(), WATCHDOG_ERROR);
      }
    }

    // Set messages if we're done.
    if ($source->progressImporting() != FEEDS_BATCH_COMPLETE) {
      return;
    }
    $info = $this->entityInfo();
    $tokens = array(
      '@entity' => strtolower($info['label']),
      '@entities' => strtolower($info['label plural']),
    );
    $messages = array();
    if ($state->created) {
      $messages[] = array(
       'message' => format_plural(
          $state->created,
          'Created @number @entity.',
          'Created @number @entities.',
          array('@number' => $state->created) + $tokens
        ),
      );
    }
    if ($state->failed) {
      $messages[] = array(
       'message' => format_plural(
          $state->failed,
          'Failed importing @number @entity.',
          'Failed importing @number @entities.',
          array('@number' => $state->failed) + $tokens
        ),
        'level' => WATCHDOG_ERROR,
      );
    }
    if (empty($messages)) {
      $messages[] = array(
        'message' => t('There are no new @entities.', array('@entities' => strtolower($info['label plural']))),
      );
    }
    foreach ($messages as $message) {
      drupal_set_message($message['message']);
      $source->log('import', $message['message'], array(), isset($message['level']) ? $message['level'] : WATCHDOG_INFO);
    }
  }

  /**
   * Override parent::configDefaults().
   */
  public function configDefaults() {

    // Start with the default processor.
    $defaults = array(
      'processor' => '',
      'processor_settings' => array(),
      'feeds_review' => array(),
    ) + parent::configDefaults();

    // Return the defaults.
    return $defaults;
  }

  /**
   * Override parent::configForm().
   */
  public function configForm(&$form_state) {

    // Ensure that Ajax didn't kill the importer.
    $this->ensureImporter();

    // Get a list of processor plugins.
    $processors = FeedsPlugin::byType('processor');

    // Build a list of processor options.
    $processor_options = array('' => '-- Select --');
    foreach ($processors as $class => $processor) {
      $processor_options[$class] = $processor['name'];
    }

    // Remove the FeedsReviewProcessor.
    unset($processor_options['FeedsReviewProcessor']);

    // If a processor was chosen in the form, set it as the configured processor, and load it into $this->processor.
    if (isset($form_state['values']['processor']) && $form_state['values']['processor'] != $this->config['processor']) {
      $this->config['processor'] = $form_state['values']['processor'];
      $this->config['processor_settings'] = array();
      $this->setConfig($this->config);
    }

    // Select list for the import's processor.
    $form['processor'] = array(
      '#type' => 'select',
      '#title' => t('Processor'),
      '#description' => t('Select the processor that imported items will be processed with after review.'),
      '#options' => $processor_options,
      '#default_value' => $this->config['processor'],
      '#required' => TRUE,
      '#ajax' => array(
        'callback' => 'feeds_review_processor_settings_ajax',
        'wrapper' => 'processor-settings',
      ),
    );

    // Create an empty fieldset for the processor's settings.
    $form['processor_settings'] = array(
      '#type' => 'fieldset',
      '#title' => t('Processor settings'),
      '#description' => t('Settings for the processor that imported items will be processed with after review.'),
      '#prefix' => '<div id="processor-settings">',
      '#suffix' => '</div>',
      '#tree' => TRUE,
    );

    // If a processor has been selected, display it's config form.
    if (!empty($this->config['processor'])) {

      // Add the processor's configuration form to the fieldset.
      $form['processor_settings'] += $this->processor->configForm($form_state);
    }

    return $form;
  }

  /**
   * Override parent::configFormValidate().
   */
  public function configFormValidate(&$values) {

    // If a processor is already set, execute it's validation function.
    if (!empty($this->config['processor'])) {
      $this->processor->configFormValidate($values['processor_settings']);
    }

    // Also use the parent method.
    parent::configFormValidate($values);
  }

  /**
   * Override parent::configFormSubmit().
   */
  public function configFormSubmit(&$values) {

    // Ensure that Ajax didn't kill the importer.
    $importer = $this->ensureImporter();

    // Set the values of the processor in the importer (this is related to the Ajax issue above).
    $importer->processor->setConfig($values);

    // If a processor is already set, execute it's submit function.
    if (!empty($this->config['processor'])) {
      $this->processor->configFormSubmit($values['processor_settings']);
    }

    // If a processor isn't set, use the parent method.
    else {
      parent::configFormSubmit($values);
    }
  }

  /**
   * Override parent::getMappingTargets().
   */
  public function getMappingTargets() {

    // If a processor is already set, load it's targets.
    if (!empty($this->config['processor'])) {
      return $this->processor->getMappingTargets();
    }

    // Otherwise, use the parent processor's mapping targets.
    else {
      return parent::getMappingTargets();
    }
  }
}