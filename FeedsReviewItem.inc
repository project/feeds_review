<?php
/**
 * @file
 * Feeds Review item entity class
 */

/**
 * The class for FeedsReviewItem entities.
 */
class FeedsReviewItem extends Entity {

  public function __construct($values = array()) {
    parent::__construct($values, 'feeds_review_item');
  }
}

/**
 * The controller for FeedsReviewItem entities
 */
class FeedsReviewItemController extends EntityAPIController {

  /**
   * Create an item - we first set up the values that are specific
   * to our feeds_review_item schema but then also go through the EntityAPIController
   * function.
   *
   * @param $values
   *   Values passed in.
   *
   * @return
   *   An item with all default fields initialized.
   */
  public function create(array $values = array()) {

    // Add values that are specific to our item.
    $values += array(
      'id' => '',
      'uid' => 0,
      'data' => array(),
      'import_id' => 0,
    );

    $item = parent::create($values);
    return $item;
  }

  /**
   * Overridden.
   * @see EntityAPIController#load($ids, $conditions)
   *
   * In addition to the parent implementation we unserialize the item's data array.
   */
  public function load($ids = array(), $conditions = array()) {

    // Start by loading entities via the parent class's method.
    $entities = parent::load($ids, $conditions);

    // Unserialize the data array in each.
    if (!empty($entities)) {
      foreach ($entities as $entity) {

        // Only unserialize if it isn't already an array.
        if (!is_array($entity->data)) {
          $entity->data = unserialize($entity->data);
        }
      }
    }

    // Return the entities.
    return $entities;
  }

  /**
   * Overridden.
   * @see EntityAPIController#buildQuery($ids, $conditions, $revision_id)
   *
   * In addition to the parent implementation we add some fields from the {feeds_item} table.
   */
  protected function buildQuery($ids, $conditions = array(), $revision_id = FALSE) {

    // If 'importer' or 'imported' are in the conditions, pop them out for later.
    if (isset($conditions['importer'])) {
      $importer = $conditions['importer'];
      unset($conditions['importer']);
    }
    if (isset($conditions['imported'])) {
      $imported = $conditions['imported'];
      unset($conditions['imported']);
    }

    // Start with the query built by the parent class's method.
    $query = parent::buildQuery($ids, $conditions, $revision_id);

    // Add the importer id and timestamp fields from the {feeds_item} table.
    $query->join('feeds_item', 'fi', 'base.id = fi.entity_id');
    $query->condition('fi.entity_type', 'feeds_review_item');
    $query->addField('fi', 'id', 'importer');
    $query->addField('fi', 'imported');

    // If 'importer' was specified, filter in the {feeds_item} table.
    if (!empty($importer)) {
      $query->condition('fi.id', $importer);
    }

    // If 'imported' was specified, filter in the {feeds_item} table.
    if (isset($imported)) {
      $query->condition('fi.imported', $imported);
    }

    // Return the query.
    return $query;
  }
}