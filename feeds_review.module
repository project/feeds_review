<?php

/**
 * @file
 * Feeds Review - Review items imported with Feeds before they are processed.
 */

/****************************************************************
 * Drupal hooks
 * **************************************************************/

/**
 * Implements hook_entity_info().
 */
function feeds_review_entity_info() {
  $entities['feeds_review_item'] = array(
    'label' => t('Feeds review item'),
    'label plural' => t('Feeds review items'),
    'entity class' => 'FeedsReviewItem',
    'controller class' => 'FeedsReviewItemController',
    'base table' => 'feeds_review_item',
    'fieldable' => FALSE,
    'entity keys' => array(
      'id' => 'id',
    ),
    'label callback' => 'entity_class_label',
    'uri callback' => 'entity_class_uri',
    'module' => 'feeds_review',
    'bundles' => array(
      'feeds_review_item' => array(
        'label' => t('Feeds item', array(), array('context' => 'a Feeds item')),
      ),
    ),
  );
  return $entities;
}

/****************************************************************
 * Ctools hooks
 * **************************************************************/

/**
 * Implements hook_feeds_plugins().
 */
function feeds_review_feeds_plugins() {
  $info = array();
  $info['FeedsReviewProcessor'] = array(
    'name' => 'Feeds Review processor',
    'description' => 'Stores Feeds items in a queue so they can be reviewed before passing them along to another processor.',
    'handler' => array(
      'parent' => 'FeedsProcessor',
      'class' => 'FeedsReviewProcessor',
      'file' => 'FeedsReviewProcessor.inc',
      'path' => drupal_get_path('module', 'feeds_review'),
    ),
  );
  return $info;
}

/***************************************************************
 * Feeds review item functions
 * *************************************************************/

/**
 * Returns a FeedsReviewItemController.
 */
function feeds_review_item() {
  return entity_get_controller('feeds_review_item');
}

/**
 * Returns an initialized Feeds Review item.
 *
 * @return
 *   A feeds item with all default fields initialized.
 */
function feeds_review_item_new($values = array()) {
  return feeds_review_item()->create($values);
}

/**
 * Loads an item by ID.
 */
function feeds_review_item_load($id) {
  $items = feeds_review_item_load_multiple(array($id), array());
  return $items ? reset($items) : FALSE;
}

/**
 * Loads multiple items by ID or based on a set of matching conditions.
 *
 * @see entity_load()
 *
 * @param $ids
 *   An array of item IDs.
 * @param $conditions
 *   An array of conditions on the {feeds_review_item} table in the form
 *     'field' => $value.
 * @param $reset
 *   Whether to reset the internal item loading cache.
 *
 * @return
 *   An array of items indexed by id.
 */
function feeds_review_item_load_multiple($ids = array(), $conditions = array(), $reset = FALSE) {
  return entity_load('feeds_review_item', $ids, $conditions, $reset);
}

/**
 * Saves an item to the database.
 *
 * @param $item
 *   The item to save.
 */
function feeds_review_item_save($item) {
  return feeds_review_item()->save($item);
}

/**
 * Deletes an item by ID.
 *
 * @param $id
 *   The ID of the item to delete.
 *
 * @return
 *   TRUE on success, FALSE otherwise.
 */
function feeds_review_item_delete($id) {
  return feeds_review_item_delete_multiple(array($id));
}

/**
 * Deletes multiple items by ID.
 *
 * @param $ids
 *   An array of item IDs to delete.
 *
 * @return
 *   TRUE on success, FALSE otherwise.
 */
function feeds_review_item_delete_multiple($ids) {
  return feeds_review_item()->delete($ids);
}

/***************************************************************
 * Form functions
 * *************************************************************/

/**
 * General purpose review form.
 * Presents a list of review items (optionally filtered), with checkboxes next to each and a button to process them.
 */
function feeds_review_form($form, &$form_state, $uid = NULL, $timestamp = NULL) {

  // Get a list of review items.
  $items = feeds_review_item_load_multiple(FALSE, $uid, $timestamp);

  // Build a list of the data fields, based on those available in the review items.
  $data_fields = array();
  foreach ($items as $item) {

    // Loop through all the array items.
    if (!empty($item->data) && is_array($item->data)) {
      foreach ($item->data as $data_field => $value) {

        // Add the field to the list of fields, if it isn't already.
        if (!in_array($data_field, $data_fields)) {
          $data_fields[] = $data_field;
        }
      }
    }
  }

  // Build a header array, with a column for the item's imported timestamp.
  $header = array(
    'imported' => t('Uploaded'),
  );

  // Add header labels for each of the data fields.
  foreach ($data_fields as $data_field) {
    $header[$data_field] = t($data_field);
  }

  // Build the rows.
  $options = array();
  foreach ($items as $item) {

    // Add an array for the item.
    $options[$item->id] = array();

    // Add the timestamp that this item was imported.
    $options[$item->id]['imported'] = format_date($item->imported);

    // Add the data from each of the data fields.
    foreach ($data_fields as $data_field) {

      // Set up the default value.
      $default_value = !empty($item->data[$data_field]) ? $item->data[$data_field] : '';
      if (is_array($default_value)) {
        $default_value = serialize($default_value);
      }
      $default_value = check_plain($default_value);

      // Add a textfield.
      $options[$item->id][$data_field] = $default_value;
    }
  }

  // Display the list as a tableselect.
  $form['items'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#empty' => t('No items available'),
  );

  // Form actions.
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['process'] = array(
    '#type' => 'submit',
    '#value' => t('Import selected items'),
    '#submit' => array('feeds_review_form_process_submit'),
  );
  $form['actions']['delete'] = array(
    '#type' => 'submit',
    '#value' => t('Delete selected items'),
    '#submit' => array('feeds_review_form_delete_submit'),
  );

  // Return the form.
  return $form;
}

/**
 * Import button submit handler for feeds_review_form().
 */
function feeds_review_form_process_submit(&$form, &$form_state) {

  // Assemble an array of selected item ids.
  $ids = array();
  if (!empty($form_state['values']['items'])) {
    foreach ($form_state['values']['items'] as $id => $item) {
      if (!empty($item)) {
        $ids[] = $id;
      }
    }
  }

  // Process the items.
  feeds_review_process_items($ids);
}

/**
 * Import button submit handler for feeds_review_form().
 */
function feeds_review_form_delete_submit(&$form, &$form_state) {

  // Assemble an array of selected item ids.
  $ids = array();
  if (!empty($form_state['values']['items'])) {
    foreach ($form_state['values']['items'] as $id => $item) {
      if (!empty($item)) {
        $ids[] = $id;
      }
    }
  }

  // Delete the items.
  feeds_review_item_delete_multiple($ids);

  // Print a message.
  drupal_set_message(t(':count items were deleted.', array(':count' => count($ids))));
}

/***************************************************************
 * Helper functions
 * *************************************************************/

/**
 * Processor settings ajax callback
 */
function feeds_review_processor_settings_ajax($form, $form_state) {
  return $form['processor_settings'];
}

/**
 * Create a new record in {feeds_review_import}.
 *
 * @param $importer_id
 *   The id of the importer being used.
 * @param $fid
 *   The id of the file to use as a source (optional).
 *
 * @return
 *   Returns an array representing the newly created record.
 */
function feeds_review_import_record($importer_id, $fid = NULL) {

  // Load the importer.
  $importer = feeds_importer($importer_id);

  // Build a record to save to {feeds_review_import} that represents information
  // about this specific import, including it's current configuration
  // (this will be used when the items are ultimately processed).
  $import = array(
    'id' => NULL,
    'config' => serialize($importer->getConfig()),
    'fid' => NULL,
    'imported' => time(),
  );

  // If a file is provided, add it to the importer so that it is saved to the database.
  if ($fid) {
    $import['fid'] = $fid;
  }

  // Save a record to the {feeds_review_importer} database.
  drupal_write_record('feeds_review_import', $import);

  // Return the newly created record.
  return $import;
}

/**
 * Run a Feeds Importer via Batch API using the Feeds Review processor.
 *
 * This function executes a custom batch operation that utilizes the
 * feeds_review_batch() callback, which is similar to the normal feed_batch()
 * callback, but performs some special trickery to convert the importer's
 * processor to FeedsReviewProcessor.
 *
 * This function should only be used if you want to force a particular import
 * to be performed using the Feeds Review processor, without altering the
 * original importer.
 *
 * In most cases, it's easier to simply configure the importer to use the
 * Feeds Review processor like you normally would. This function is handy if you
 * want to do more fancy behind-the-scenes stuff, or if you don't necessarily
 * know which importer is going to be used.
 *
 * @param $importer_id
 *   The id of the Feeds Importer to run.
 * @param $fid
 *   The id file to use as a source (optional).
 * @param $override
 *   Whether or not to override the importer so it used Feeds Review Processor.
 *   Defaults to TRUE.
 */
function feeds_review_batch_import($importer_id, $fid = NULL, $override = TRUE) {

  // Create a new import record in the database.
  $import = feeds_review_import_record($importer_id, $fid);

  // Start the import via our own Batch API function.
  $batch = array(
    'title' => 'Importing to Ledger',
    'operations' => array(
      array('feeds_review_batch', array($importer_id, $import['id'], $override)),
    ),
    'progress_message' => '',
  );
  batch_set($batch);
}

/**
 * Feeds Review Batch API worker callback.
 *
 * This is essentially the same thing as feeds_batch(), except that it uses the
 * function feeds_review_source to load and alter the source so that it uses
 * the Feeds Review processor.
 *
 * @see feeds_review_batch_import().
 *
 * @param $importer_id
 *   Identifier of a FeedsImporter object.
 * @param $import_id
 *   The ID of the import.
 * @param $override
 *   Whether or not to override the importer so it used Feeds Review Processor.
 * @param $context
 *   Batch context.
 */
function feeds_review_batch($importer_id, $import_id, $override, &$context) {
  $context['finished'] = FEEDS_BATCH_COMPLETE;
  try {
    $context['finished'] = feeds_review_source($importer_id, $import_id, $override)->import();
  }
  catch (Exception $e) {
    drupal_set_message($e->getMessage(), 'error');
  }
}

/**
 * Gets an instance of a source object, and changes the importer's processor
 * to FeedsReviewProcessor.
 *
 * @see feeds_review_batch().
 *
 * @param $importer_id
 *   A FeedsImporter id.
 * @param $import_id
 *   The id of the import in the {feeds_review_import} table.
 * @param $override
 *   Whether or not to override the importer so it used Feeds Review Processor.
 *   Defaults to TRUE.
 *
 * @return
 *   A FeedsSource object.
 */
function feeds_review_source($importer_id, $import_id, $override = TRUE) {

  // Load an instance of the Feeds Source object of the importer.
  $source = feeds_source($importer_id, 0);

  // If the $override argument is TRUE, override the importer's processor.
  if ($override) {

    // Set the processor to Feeds Review Processor and copy the config.
    _feeds_review_override_processor($source->importer);

    // Add the import ID to the processor configuration.
    // This is used in FeedsReviewProcessor->process() when items are created.
    $processor_config = array(
      'feeds_review' => array(
        'import_id' => $import_id
      ),
    );
    $source->importer->processor->addConfig($processor_config);
  }

  // Load the file id of the import.
  $query = db_select('feeds_review_import', 'fri');
  $query->addField('fri', 'fid');
  $query->condition('id', $import_id);
  $result = $query->execute();
  $fid = $result->fetchField();

  // If a file id is set, load the file object and replace the file in the source.
  if ($fid) {

    // Load the file.
    $file = file_load($fid);

    // Load the source fetcher config.
    $fetcher_config = $source->getConfigFor($source->importer->fetcher);

    // Add the uploaded file to the fetcher, so that it is passed to the parser, and saved for future reference.
    $fetcher_config['source'] = $file->uri;
    $fetcher_config['file'] = $file;

    // Set the source fetcher config.
    $source->setConfigFor($source->importer->fetcher, $fetcher_config);
  }

  // Return the source object.
  return $source;
}

/**
 * Helper function for overriding the processor of an importer.
 * Sets the processor to FeedsReviewProcessor and copies over the original
 * configuration.
 *
 * @param $importer
 *   A Feeds Importer object to override.
 *
 * @return
 *   Returns the modified Feeds Importer object.
 */
function _feeds_review_override_processor(&$importer) {

  // Get the current processor's configuration.
  $config = $importer->processor->getConfig();

  // Change the processor of the importer to FeedsReviewProcessor.
  $importer->setPlugin('FeedsReviewProcessor');

  // Get the new processor's configuration (they will just be defaults).
  $new_config = $importer->processor->getconfig();

  // Store the original processor's machine name in the config.
  $new_config['processor'] = $importer->config['processor']['plugin_key'];

  // Copy the mappings from the original processor to the temporary one.
  $new_config['mappings'] = $config['mappings'];

  // Store the rest of the original processor's settings in the config.
  $new_config['processor_settings'] = $config;

  // Set the processor's config.
  $importer->processor->setConfig($new_config);

  // Return the overridden importer.
  return $importer;
}

/**
 * Helper function for reverting the processor of an importer.
 * Sets the processor to whatever is selected as the final processor, in the
 * Feeds Review Processor configuration. Copies the original configuration also.
 *
 * @param $importer
 *   A Feeds Importer object to revert.
 *
 * @return
 *   Returns the modified Feeds Importer object.
 */
function _feeds_review_revert_processor(&$importer) {

  // Get the current processor's configuration.
  $config = $importer->processor->getConfig();

  // Change the processor of the importer to the processor specified in config.
  $importer->setPlugin($config['processor']);

  // Set the processor's config.
  $importer->processor->setConfig($config['processor_settings']);

  // Return the overridden importer.
  return $importer;
}

/**
 * Process a set of Feeds Review items with their final processor.
 *
 * @param $ids
 *   An array of $ids to process.
 */
function feeds_review_process_items($ids) {

  // Make sure ids were provided.
  if (empty($ids) || !is_array($ids)) {
    return;
  }

  // Start the processing via our own Batch API function.
  $batch = array(
    'title' => 'Importing to Ledger',
    'operations' => array(
      array('feeds_review_process_batch', array($ids)),
    ),
    'progress_message' => '',
  );
  batch_set($batch);
}

/**
 * Batch API callback for processing Feeds Review items with their final processor.
 *
 * $ids
 *   An array of $ids to process.
 * $context
 *   The context from Batch API.
 */
function feeds_review_process_batch($ids, &$context) {

  // Start an array counter to remember where we are in the ids.
  if (!isset($context['sandbox']['i'])) {
    $context['sandbox']['i'] = 0;
  }

  // Count the ids.
  $count_ids = count($ids);

  // Store loaded import information.
  $context['sandbox']['imports'] = array();
  $imports = &$context['sandbox']['imports'];

  // Process the next X items (according to the Feeds process limit)
  for ($i=$context['sandbox']['i'];$i<variable_get('feeds_process_limit', 50);$i++) {

    // Make sure $i is less than the size of $ids.
    if ($i < $count_ids) {

      // Load the item from the database.
      if ($item = feeds_review_item_load($ids[$i])) {
        try {

          // Load the import data, if it hasn't been already.
          if (!isset($imports[$item->import_id])) {

            // Load the import data from the database.
            $query = db_select('feeds_review_import', 'fri');
            $query->fields('fri');
            $query->condition('id', $item->import_id);
            $result = $query->execute();
            $import = $result->fetchAssoc();

            // Unserialize the configuration.
            if (!empty($import['config'])) {
              $import['config'] = unserialize($import['config']);
            }

            // Save the import data to the static array.
            $imports[$item->import_id] = $import;
          }

          // Load the original importer's source object.
          $source = feeds_source($item->importer);

          // Set the configuration of the importer to the configuration saved
          // from the original import.
          $source->importer->setConfig($imports[$item->import_id]['config']);

          // If the importer is configured to use the Feeds Review Processor,
          // revert it to it's final processor and configuration instead.
          if ($source->importer->config['processor']['plugin_key'] == 'FeedsReviewProcessor') {
            _feeds_review_revert_processor($source->importer);
          }

          // Save the item's data as a parser result.
          $parser_result = new FeedsParserResult(array($item->data));

          // Process.
          $source->importer->processor->process($source, $parser_result);

          // Delete the Feeds Review item.
          feeds_review_item_delete($item->id);
        }
        catch (Exception $e) {
          // Do nothing.
        }
      }

      // Set the global counter.
      $context['sandbox']['i'] = $i;
    }
  }

  // We're finished when all the ids have been processed.
  $context['finished'] = ($context['sandbox']['i'] + 1) / $count_ids;
}
